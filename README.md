# Nanairo (7 colors) #

### What is this repository for? ###

* Tile game written in Java
* Version: beta
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Rules ###
Goal is to score highest points possible by clicking colored capsules (bubbles). In order to score
you have to click on one that is connected to at least one vertical or holizontally connected capsules.

### Unit Test coverage (Jacoco)

    ./gradlew build jacocoTestReport

Generated report files can be found in `build/reports/jacoco/test/html/index.html`


### How do I get set up? ###

Use gradle to build and run.

    ./gradlew run

### Who do I talk to? ###

* Repo owner