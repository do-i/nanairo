package com.djd.fun.nanairo.state;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.base.Location2D;
import com.djd.fun.nanairo.factory.CapsuleGridFactory;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.CapsuleImpl;
import com.djd.fun.nanairo.model.Color;
import com.djd.fun.nanairo.model.GravityPosition;
import com.djd.fun.nanairo.service.GridService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(MockitoExtension.class)
class GameGridTest extends Mockito {

  private static final Capsule R = CapsuleImpl.builder().color(Color.Red).build();
  private static final Capsule G = CapsuleImpl.builder().color(Color.Green).build();
  private static final Capsule B = CapsuleImpl.builder().color(Color.Blue).build();

  private @Mock CapsuleGridFactory capsuleGridFactory;
  private GameGrid gameGrid;

  @BeforeEach
  void setUp() {

    Capsule[][] capsules = {
        {B, B, B, G, R},
        {B, G, G, G, R},
        {G, B, R, G, R},
        {G, R, R, R, G}
    };

    doReturn(capsules).when(capsuleGridFactory).createRandomGrid();
    gameGrid = new GameGrid(180, 3, 100, capsuleGridFactory,
        new GridService(ImmutableSize.of(capsules[0].length, capsules.length)),
        Clock.fixed(Instant.ofEpochSecond(10000), ZoneId.systemDefault()));
  }

  @Test
  void increaseTimeBy() {
    gameGrid.increaseTimeBy(60);
    assertThat(gameGrid.getActiveRemainingDuration().getSeconds()).isEqualTo(181);
  }

  @Test
  void multiplyPointsBy() {
    gameGrid.multiplyPointsBy(2);
    assertThat(gameGrid.getActivePoints()).isEqualTo(200);
  }

  @Test
  void reducePointsBy() {
    gameGrid.reducePointsBy(2);
    assertThat(gameGrid.getActivePoints()).isEqualTo(50);
  }

  @Test
  void reverseGravity() {
    gameGrid.reverseGravity();
    assertThat(gameGrid.getActiveGravityPosition()).isEqualTo(GravityPosition.Kita);
    gameGrid.reverseGravity();
    assertThat(gameGrid.getActiveGravityPosition()).isEqualTo(GravityPosition.Mina);
  }

  @Test
  void shiftGravityClockwise() {
    gameGrid.rotateGravityForward();
    assertThat(gameGrid.getActiveGravityPosition()).isEqualTo(GravityPosition.Nish);
  }

  @Test
  void shiftGravityCounterClockwise() {
    gameGrid.rotateGravityBackward();
    assertThat(gameGrid.getActiveGravityPosition()).isEqualTo(GravityPosition.Higa);
  }

  @Test
  void getCapsuleAt() {
    assertThat(gameGrid.getCapsuleAt(Location2D.of(3, 4))).isEqualTo(G);
  }

  @Test
  void findConnectedLocations() {
    assertThat(gameGrid.findConnectedLocations(Location2D.of(1, 1))).containsExactly(
        Location2D.of(1, 1), Location2D.of(1, 2),
        Location2D.of(2, 3), Location2D.of(1, 3), Location2D.of(0, 3));
  }
}