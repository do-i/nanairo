package com.djd.fun.nanairo.factory;

import java.util.Random;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.Color;
import com.djd.fun.nanairo.model.Trait;
import com.google.common.collect.ImmutableSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class CapsuleGridFactoryTest {
  private static final ImmutableSize GRID_SIZE = ImmutableSize.of(28, 7);
  private static final double TRAIT_GRID_RATIO = 0.3;
  private CapsuleGridFactory capsuleGridFactory;

  @BeforeEach
  void setUp() {
    capsuleGridFactory = new CapsuleGridFactory(
        new Random(8), TRAIT_GRID_RATIO, GRID_SIZE);
  }

  @Test
  void createUniqueCapsules() {
    assertThat(CapsuleGridFactory.createEveryCapsules()).hasSize(Color.size() * Trait.size());
  }

  @Test
  void createNoOpTraitCapsules() {
    ImmutableSet<Capsule> capsules = CapsuleGridFactory.createNoTraitCapsules();
    assertThat(capsules).hasSize(Color.size());
    capsules.forEach(capsule -> assertThat(capsule.getTrait()).isEqualTo(Trait.NoOp));
  }

  @Test
  void createRandomGrid() {
    int withoutTraitCounter = 0;
    int specialTraitCounter = 0;
    Capsule[][] capsuleGrid = capsuleGridFactory.createRandomGrid();
    for (int row = 0; row < GRID_SIZE.getHeight(); row++) {
      for (int col = 0; col < GRID_SIZE.getWidth(); col++) {
        Capsule capsule = capsuleGrid[row][col];
        if (capsule.getTrait() == Trait.NoOp) {
          ++withoutTraitCounter;
        } else {
          ++specialTraitCounter;
        }
      }
    }
    assertThat(specialTraitCounter).isLessThan(withoutTraitCounter);
  }
}