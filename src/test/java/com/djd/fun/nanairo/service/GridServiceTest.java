package com.djd.fun.nanairo.service;

import java.util.Arrays;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.base.Location2D;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.CapsuleImpl;
import com.djd.fun.nanairo.model.Color;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

class GridServiceTest {

  private static final Capsule R = CapsuleImpl.builder().color(Color.Red).build();
  private static final Capsule G = CapsuleImpl.builder().color(Color.Green).build();
  private static final Capsule B = CapsuleImpl.builder().color(Color.Blue).build();
  private static final Capsule E = Capsule.EMPTY;

  private GridService gridService;

  @BeforeEach
  void setUp() {
    gridService = new GridService(ImmutableSize.of(3, 3));
  }

  @Test
  void findAdjacentLocations_topLeft_3() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(0, 0)))
        .containsExactly(
            Location2D.of(0, 1),
            Location2D.of(1, 0),
            Location2D.of(1, 1)
        );
  }

  @Test
  void findAdjacentLocations_topRight_3() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(0, 2)))
        .containsExactly(
            Location2D.of(0, 1),
            Location2D.of(1, 1),
            Location2D.of(1, 2)
        );
  }

  @Test
  void findAdjacentLocations_bottomLeft_3() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(2, 0)))
        .containsExactly(
            Location2D.of(1, 0),
            Location2D.of(1, 1),
            Location2D.of(2, 1)
        );
  }

  @Test
  void findAdjacentLocations_bottomRight_3() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(2, 2)))
        .containsExactly(
            Location2D.of(1, 2),
            Location2D.of(1, 1),
            Location2D.of(2, 1)
        );
  }

  @Test
  void findAdjacentLocations_left_5() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(1, 0)))
        .containsExactly(
            Location2D.of(0, 0),
            Location2D.of(0, 1),
            Location2D.of(1, 1),
            Location2D.of(2, 1),
            Location2D.of(2, 0)
        );
  }

  @Test
  void findAdjacentLocations_top_5() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(0, 1)))
        .containsExactly(
            Location2D.of(0, 0),
            Location2D.of(0, 2),
            Location2D.of(1, 0),
            Location2D.of(1, 1),
            Location2D.of(1, 2)
        );
  }

  @Test
  void findAdjacentLocations_right_5() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(1, 2)))
        .containsExactly(
            Location2D.of(0, 1),
            Location2D.of(0, 2),
            Location2D.of(1, 1),
            Location2D.of(2, 1),
            Location2D.of(2, 2)
        );
  }

  @Test
  void findAdjacentLocations_bottom_5() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(2, 1)))
        .containsExactly(
            Location2D.of(1, 0),
            Location2D.of(1, 1),
            Location2D.of(1, 2),
            Location2D.of(2, 0),
            Location2D.of(2, 2)
        );
  }

  @Test
  void findAdjacentLocations_center_8() {
    assertThat(gridService.findAdjacentLocations(Location2D.of(1, 1)))
        .containsExactly(
            Location2D.of(0, 0),
            Location2D.of(0, 1),
            Location2D.of(0, 2),
            Location2D.of(1, 0),
            Location2D.of(1, 2),
            Location2D.of(2, 0),
            Location2D.of(2, 1),
            Location2D.of(2, 2)
        );
  }

  @Test
  void findCrossAdjacentLocations_center() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(1, 1)))
        .containsExactly(
            Location2D.of(0, 1),
            Location2D.of(1, 0),
            Location2D.of(1, 2),
            Location2D.of(2, 1)
        );
  }

  @Test
  void findCrossAdjacentLocations_topLeft() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(0, 0)))
        .containsExactly(
            Location2D.of(0, 1),
            Location2D.of(1, 0)
        );
  }

  @Test
  void findCrossAdjacentLocations_topRight() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(0, 2)))
        .containsExactly(
            Location2D.of(0, 1),
            Location2D.of(1, 2)
        );
  }

  @Test
  void findCrossAdjacentLocations_bottomLeft() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(2, 0)))
        .containsExactly(
            Location2D.of(1, 0),
            Location2D.of(2, 1)
        );
  }

  @Test
  void findCrossAdjacentLocations_bottomRight() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(2, 2)))
        .containsExactly(
            Location2D.of(1, 2),
            Location2D.of(2, 1)
        );
  }


  @Test
  void findCrossAdjacentLocations_top() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(0, 1)))
        .containsExactly(
            Location2D.of(0, 0),
            Location2D.of(0, 2),
            Location2D.of(1, 1)
        );
  }

  @Test
  void findCrossAdjacentLocations_right() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(1, 2)))
        .containsExactly(
            Location2D.of(0, 2),
            Location2D.of(1, 1),
            Location2D.of(2, 2)
        );
  }

  @Test
  void findCrossAdjacentLocations_bottom() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(2, 1)))
        .containsExactly(
            Location2D.of(1, 1),
            Location2D.of(2, 0),
            Location2D.of(2, 2)
        );
  }

  @Test
  void findCrossAdjacentLocations_left() {
    assertThat(gridService.findCrossAdjacentLocations(Location2D.of(1, 0)))
        .containsExactly(
            Location2D.of(0, 0),
            Location2D.of(1, 1),
            Location2D.of(2, 0)
        );
  }

  @Test
  void applyGravityMina() {
    Capsule[][] input = {
        {B, B, B},
        {B, G, G},
        {G, E, E}
    };
    Capsule[][] expected = {
        {B, E, E},
        {B, B, B},
        {G, G, G}
    };
    Capsule[][] output = gridService.applyGravityMina(input);
    assertThat(Arrays.deepEquals(output, expected)).isTrue();
  }

  @Test
  void applyGravityKita() {
    Capsule[][] input = {
        {B, E, E},
        {B, B, B},
        {G, G, G}
    };
    Capsule[][] expected = {
        {B, B, B},
        {B, G, G},
        {G, E, E}
    };
    Capsule[][] output = gridService.applyGravityKita(input);
    assertThat(Arrays.deepEquals(output, expected)).isTrue();
  }

  @Test
  void applyGravityHiga() {
    Capsule[][] input = {
        {B, E, G},
        {B, B, E},
        {E, B, G}
    };
    Capsule[][] expected = {
        {E, B, G},
        {E, B, B},
        {E, B, G}
    };
    Capsule[][] output = gridService.applyGravityHiga(input);
    assertThat(Arrays.deepEquals(output, expected)).isTrue();
  }

  @Test
  void applyGravityNish() {
    Capsule[][] input = {
        {B, E, G},
        {B, B, E},
        {E, B, G}
    };
    Capsule[][] expected = {
        {B, G, E},
        {B, B, E},
        {B, G, E}
    };
    Capsule[][] output = gridService.applyGravityNish(input);
    assertThat(Arrays.deepEquals(output, expected)).isTrue();
  }
}