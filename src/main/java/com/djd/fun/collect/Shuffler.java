package com.djd.fun.collect;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class Shuffler {

  // no instance
  private Shuffler() {}

  /**
   * Selects specified number of elements from source randomly. if given count is greater than source
   * size, then resulting list will contain duplicates.
   *
   * @param source {@link ImmutableCollection} of items chosen from
   * @param count number of resulting set size
   * @param random {@link Random} used for shuffle
   * @param <T> type of elements in the source
   * @return {@link ImmutableList} of randomly selected items
   */
  public static <T> ImmutableList<T> selectRandomSubList(ImmutableCollection<T> source, int count,
      Random random) {
    if (source.isEmpty() || count < 1) {
      return ImmutableList.of();
    }
    List<T> ephemeralList = Lists.newArrayList(source);
    int duplicateCount = (count - 1) / source.size();
    for (int i = 0; i < duplicateCount; i++) {
      ephemeralList.addAll(source);
    }
    Collections.shuffle(ephemeralList, random);
    return ImmutableList.copyOf(ephemeralList.subList(0, count));
  }
}
