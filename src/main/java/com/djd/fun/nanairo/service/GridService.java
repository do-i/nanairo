package com.djd.fun.nanairo.service;

import javax.inject.Inject;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.base.Location2D;
import com.djd.fun.nanairo.annotation.Grid;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.GravityPosition;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

public class GridService {

  private final ImmutableSize gridSize;

  @Inject
  public GridService(@Grid ImmutableSize gridSize) {
    this.gridSize = gridSize;
  }

  /**
   * Finds surrounding locations by the given location. The minimum number locations is 3 if
   * given location is one of four corners. Five locations will be returned if given location is
   * on the edge. Otherwise the method will always returns 8 locations.
   *
   * @param location
   * @return Either 3, 5, or 8 locations surrounding the given location if location is within the boundary.
   * Empty list if the location is out side of boundary.
   */
  public ImmutableList<Location2D> findAdjacentLocations(Location2D location) {
    ImmutableList.Builder<Location2D> adjacentLocations = ImmutableList.builder();
    for (int r = -1; r <= 1; r++) {
      for (int c = -1; c <= 1; c++) {
        int row = location.getRowIndex() + r;
        int col = location.getColIndex() + c;
        if (r == 0 && c == 0) {
          // skip if current location is same as given location
          continue;
        }
        if (row < 0 || col < 0 || row >= gridSize.getHeight() || col >= gridSize.getWidth()) {
          // skip out of boundary locations
          continue;
        }
        adjacentLocations.add(Location2D.of(row, col));
      }
    }
    return adjacentLocations.build();
  }

  /**
   * Finds North, East, South, and West of given location.
   *
   * @param location
   * @return Either 4, 3, or 2 locations.
   */
  public ImmutableList<Location2D> findCrossAdjacentLocations(Location2D location) {
    ImmutableList.Builder<Location2D> adjacentLocations = ImmutableList.builder();
    for (int r = -1; r <= 1; r++) {
      for (int c = -1; c <= 1; c++) {
        int row = location.getRowIndex() + r;
        int col = location.getColIndex() + c;
        if (Math.abs(r - c) != 1) {
          // skip if current location is same as given location
          continue;
        }
        if (row < 0 || col < 0 || row >= gridSize.getHeight() || col >= gridSize.getWidth()) {
          // skip out of boundary locations
          continue;
        }
        adjacentLocations.add(Location2D.of(row, col));
      }
    }
    return adjacentLocations.build();
  }

  /**
   * @param source
   * @return
   */
  public Capsule[][] applyGravity(Capsule[][] source, GravityPosition gravityPosition) {
    switch (gravityPosition) {
      case Mina:
        return applyGravityMina(source);
      case Kita:
        return applyGravityKita(source);
      case Higa:
        return applyGravityHiga(source);
      case Nish:
        return applyGravityNish(source);
      default:
        throw new UnsupportedOperationException("No case for " + gravityPosition);
    }
  }

  @VisibleForTesting Capsule[][] applyGravityMina(Capsule[][] source) {
    Capsule[][] target = createAndFillEmptyCapsules();
    for (int col = 0; col < gridSize.getWidth(); col++) {
      int tarRowidx = gridSize.getHeight() - 1;
      for (int row = gridSize.getHeight() - 1; row >= 0; row--) {
        Capsule capsule = source[row][col];
        if (capsule != Capsule.EMPTY) {
          target[tarRowidx--][col] = capsule;
        }
      }
    }
    return target;
  }

  @VisibleForTesting Capsule[][] applyGravityKita(Capsule[][] source) {
    Capsule[][] target = createAndFillEmptyCapsules();
    for (int col = 0; col < gridSize.getWidth(); col++) {
      int tarRowidx = 0;
      for (int row = 0; row < gridSize.getHeight(); row++) {
        Capsule capsule = source[row][col];
        if (capsule != Capsule.EMPTY) {
          target[tarRowidx++][col] = capsule;
        }
      }
    }
    return target;
  }

  @VisibleForTesting Capsule[][] applyGravityHiga(Capsule[][] source) {
    Capsule[][] target = createAndFillEmptyCapsules();
    for (var row = 0; row < gridSize.getHeight(); row++) {
      int tarColidx = gridSize.getWidth() - 1;
      for (var col = gridSize.getWidth() - 1; col >= 0; col--) {
        Capsule capsule = source[row][col];
        if (capsule != Capsule.EMPTY) {
          target[row][tarColidx--] = capsule;
        }
      }
    }
    return target;
  }

  @VisibleForTesting Capsule[][] applyGravityNish(Capsule[][] source) {
    Capsule[][] target = createAndFillEmptyCapsules();
    for (int row = 0; row < gridSize.getHeight(); row++) {
      int tarColidx = 0;
      for (int col = 0; col < gridSize.getWidth(); col++) {
        Capsule capsule = source[row][col];
        if (capsule != Capsule.EMPTY) {
          target[row][tarColidx++] = capsule;
        }
      }
    }
    return target;
  }

  private Capsule[][] createAndFillEmptyCapsules() {
    Capsule[][] capsules = new Capsule[gridSize.getHeight()][gridSize.getWidth()];
    for (int row = 0; row < gridSize.getHeight(); row++) {
      for (int col = 0; col < gridSize.getWidth(); col++) {
        capsules[row][col] = Capsule.EMPTY;
      }
    }
    return capsules;
  }
}
