package com.djd.fun.nanairo;

import java.time.Clock;
import java.util.Random;

import javax.inject.Singleton;
import javax.swing.JFrame;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.nanairo.annotation.BasePanelSize;
import com.djd.fun.nanairo.annotation.Cell;
import com.djd.fun.nanairo.annotation.Grid;
import com.djd.fun.nanairo.annotation.MaxScore;
import com.djd.fun.nanairo.annotation.RemainingTime;
import com.djd.fun.nanairo.annotation.ScoreFactor;
import com.djd.fun.nanairo.annotation.StatusPanelSize;
import com.djd.fun.nanairo.ui.sprite.SpriteModule;
import com.djd.fun.nanairo.ui.wireframe.MainFrame;
import com.google.common.eventbus.EventBus;
import com.google.inject.PrivateModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

public class GameModule extends PrivateModule {

  @Override
  protected void configure() {
    install(new SpriteModule());
    createAndBindEventBus();
    bind(JFrame.class).to(MainFrame.class);
    expose(JFrame.class);
  }

  @Provides @Singleton
  Clock provideClock() {
    return Clock.systemDefaultZone();
  }

  @Provides @Singleton @BasePanelSize
  ImmutableSize provideBasePanelSize(@Grid ImmutableSize gridSize, @Cell ImmutableSize cellSize) {
    return ImmutableSize.of(gridSize.getWidth() * cellSize.getWidth(),
        gridSize.getHeight() * cellSize.getHeight());
  }

  @Provides @Singleton @StatusPanelSize
  ImmutableSize provideStatusPanelSize(@BasePanelSize ImmutableSize basePanelSize) {
    return ImmutableSize.of(basePanelSize.getWidth(), 32);
  }

  @Provides @Singleton Random providePsudoRandom() {
    return new Random(2357111);
  }

  @Provides @Singleton @Grid double provideTraitGridRatio() {
    return 0.08;
  }

  @Provides @Singleton @Cell ImmutableSize provideCapsuleSize() {
    return ImmutableSize.of(32, 32);
  }

  @Provides @Singleton @Grid ImmutableSize provideGridSize() {
    return ImmutableSize.of(18, 18);
  }

  @Provides @Singleton @RemainingTime int provideInitialRemainingTime() {
    return 777;
  }

  @Provides @Singleton @MaxScore int provideMaxScore() {
    return 1_000_000 - 1;
  }
  @Provides @Singleton int provideInitialPoints() {
    return 0;
  }

  @Provides @Singleton @ScoreFactor int provideScoreFactor() {
    // number of connected capsule to the power of this value.
    return 3;
  }
  /**
   * https://spin.atomicobject.com/2012/01/13/the-guava-eventbus-on-guice/
   * This bindListener allows Guice to register all methods that are annotated with @Subscribe
   */
  private void createAndBindEventBus() {
    EventBus eventBus = new EventBus("BIG BLU BUS");
    bindListener(Matchers.any(), new TypeListener() {
      public <I> void hear(TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) {
        typeEncounter.register((InjectionListener<I>)i -> eventBus.register(i));
      }
    });
    bind(EventBus.class).toInstance(eventBus);
  }
}
