package com.djd.fun.nanairo.factory;

import java.util.EnumSet;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.base.Location2D;
import com.djd.fun.collect.Shuffler;
import com.djd.fun.nanairo.annotation.Grid;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.CapsuleImpl;
import com.djd.fun.nanairo.model.Color;
import com.djd.fun.nanairo.model.Trait;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class CapsuleGridFactory {

  private static final Logger log = LoggerFactory.getLogger(CapsuleGridFactory.class);
  private static final ImmutableList<Capsule> EVERY_CAPSULES = ImmutableList.copyOf(createEveryCapsules());
  private static final ImmutableList<Capsule> NO_TRAIT_CAPSULES = ImmutableList.copyOf(createNoTraitCapsules());
  private final ImmutableSet<Location2D> capsuleLocations;
  private final ImmutableSize gridSize;
  private final int numOfTraits;
  private final Random random;

  @Inject
  public CapsuleGridFactory(Random random, @Grid double traitGridRatio, @Grid ImmutableSize gridSize) {
    this.random = random;
    this.gridSize = gridSize;
    this.capsuleLocations = createCapsuleLocations(gridSize);
    this.numOfTraits = (int)(gridSize.getWidth() * gridSize.getHeight() * traitGridRatio);
    log.info("Factory instance created.");
  }

  public Capsule[][] createRandomGrid() {
    Capsule[][] grid = new Capsule[gridSize.getHeight()][gridSize.getWidth()];

    /* Initialize base grid with capsules without a trait */
    for (int row = 0; row < gridSize.getHeight(); row++) {
      for (int col = 0; col < gridSize.getWidth(); col++) {
        grid[row][col] = NO_TRAIT_CAPSULES.get(random.nextInt(NO_TRAIT_CAPSULES.size()));
      }
    }

    log.info("numOfTraits={}", numOfTraits);
    /* Randomly place random traits in the grid */
    ImmutableList<Location2D> selectedLocations =
        Shuffler.selectRandomSubList(capsuleLocations, numOfTraits, random);
    ImmutableList<Capsule> selectedCapsules =
        Shuffler.selectRandomSubList(EVERY_CAPSULES, numOfTraits, random);

    for (int index = 0; index < numOfTraits; index++) {
      Location2D location = selectedLocations.get(index);
      grid[location.getRowIndex()][location.getColIndex()] = selectedCapsules.get(index);
    }
    return grid;
  }

  private static ImmutableSet<Location2D> createCapsuleLocations(ImmutableSize gridSize) {
    ImmutableSet.Builder<Location2D> locations = ImmutableSet.builder();
    for (int row = 0; row < gridSize.getHeight(); row++) {
      for (int col = 0; col < gridSize.getWidth(); col++) {
        locations.add(Location2D.of(row, col));
      }
    }
    return locations.build();
  }

  @VisibleForTesting static ImmutableSet<Capsule> createNoTraitCapsules() {
    return EnumSet.allOf(Color.class).stream()
        .map(color -> CapsuleImpl.builder().color(color).build())
        .collect(ImmutableSet.toImmutableSet());
  }

  @VisibleForTesting static ImmutableSet<Capsule> createEveryCapsules() {
    ImmutableSet.Builder<Capsule> capsules = ImmutableSet.builder();
    CapsuleImpl.Builder capsule = CapsuleImpl.builder();
    for (Color color : Color.values()) {
      capsule.color(color);
      for (Trait trait : Trait.values()) {
        capsules.add(capsule.trait(trait).build());
      }
    }
    return capsules.build();
  }
}
