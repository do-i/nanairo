package com.djd.fun.nanairo.state;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.djd.fun.base.Location2D;
import com.djd.fun.nanairo.annotation.RemainingTime;
import com.djd.fun.nanairo.annotation.ScoreFactor;
import com.djd.fun.nanairo.factory.CapsuleGridFactory;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.Color;
import com.djd.fun.nanairo.model.GravityPosition;
import com.djd.fun.nanairo.service.GridService;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;
import com.google.common.math.IntMath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Predicates.not;

@Singleton
public class GameGrid implements StateMutator {

  private static final Logger log = LoggerFactory.getLogger(GameGrid.class);
  private static final int EXTRA_SECOND = 1;
  private final ReentrantReadWriteLock.WriteLock writeLock = new ReentrantReadWriteLock().writeLock();
  private final int remainingTime;
  private final int scoreFactor;
  private final int initPoints;
  private final CapsuleGridFactory capsuleGridFactory;
  private final GridService gridService;
  private final Clock clock;

  /* active state variables */

  private GravityPosition activeGravityPosition;
  private Instant deadLine;
  private final AtomicInteger activePoints = new AtomicInteger();
  private Capsule[][] capsules;

  @Inject GameGrid(@RemainingTime int remainingTime, @ScoreFactor int scoreFactor, int initPoints,
      CapsuleGridFactory capsuleGridFactory,
      GridService gridService, Clock clock) {
    this.remainingTime = remainingTime;
    this.scoreFactor = scoreFactor;
    this.initPoints = initPoints;
    this.capsuleGridFactory = capsuleGridFactory;
    this.gridService = gridService;
    this.clock = clock;
    init();
    log.info("GameGrid.init()");
  }

  public void init() {
    activePoints.set(initPoints);
    activeGravityPosition = GravityPosition.Mina;
    capsules = capsuleGridFactory.createRandomGrid();
    deadLine = Instant.now(clock).plusSeconds(remainingTime + EXTRA_SECOND);
  }

  /* mutators */

  @Override
  public void increaseTimeBy(int seconds) {
    long moreSeconds = Math.min(getActiveRemainingDuration().getSeconds() + seconds, remainingTime);
    deadLine = Instant.now(clock).plusSeconds(moreSeconds + EXTRA_SECOND);
  }

  @Override
  public void multiplyPointsBy(int factor) {
    activePoints.getAndUpdate(operand -> operand * factor);
  }

  @Override
  public void reducePointsBy(int factor) {
    activePoints.getAndUpdate(operand -> operand / factor);
  }

  @Override
  public void reverseGravity() {
    writeLock.lock();
    try {
      activeGravityPosition = activeGravityPosition.getOpposit();
    } finally {
      writeLock.unlock();
    }
    log.info("reverseGravity: {}", activeGravityPosition);
  }

  @Override
  public void rotateGravityForward() {
    writeLock.lock();
    try {
      activeGravityPosition = activeGravityPosition.getForwardPosition();
    } finally {
      writeLock.unlock();
    }
    log.info("rotateGravityForward: {}", activeGravityPosition);
  }

  @Override
  public void rotateGravityBackward() {
    writeLock.lock();
    try {
      activeGravityPosition = activeGravityPosition.getBackwardPosition();
    } finally {
      writeLock.unlock();
    }
    log.info("rotateGravityBackward: {}", activeGravityPosition);
  }

  public void handleTileSelection(Location2D location) {
    ImmutableSet<Location2D> selectedLocations = findConnectedLocations(location);
    if (selectedLocations.size() < 2) {
      // Must connect at least two capsules to eliminate.
      return;
    }
    activePoints.getAndAdd(computePoints(selectedLocations.size()));
    selectedLocations.stream()
        .map(this::getCapsuleAt)
        .map(Capsule::getTrait)
        .forEach(trait -> trait.apply(this));
    removeConnectedCapsules(selectedLocations);
    shiftTowardsGravityPossition();
  }

  /**
   * Replaces capsules on specified locations with {@link Capsule::EMPTY}
   *
   * @param locations
   */
  public void removeConnectedCapsules(ImmutableSet<Location2D> locations) {
    writeLock.lock();
    try {
      locations.forEach(location ->
          capsules[location.getRowIndex()][location.getColIndex()] = Capsule.EMPTY);
    } finally {
      writeLock.unlock();
    }
  }

  private int computePoints(int n) {
    return IntMath.pow(n, scoreFactor);
  }

  /**
   * Shifts capsules towards gravity.
   */
  private void shiftTowardsGravityPossition() {
    writeLock.lock();
    try {
      capsules = gridService.applyGravity(capsules, activeGravityPosition);
    } finally {
      writeLock.unlock();
    }
  }

  /* accessors */

  public int getActivePoints() {
    return activePoints.get();
  }

  public Duration getActiveRemainingDuration() {
    return Duration.between(Instant.now(clock), deadLine);
  }

  public GravityPosition getActiveGravityPosition() {
    return activeGravityPosition;
  }

  public Capsule getCapsuleAt(Location2D location) {
    checkElementIndex(location.getRowIndex(), capsules.length, "Row");
    checkElementIndex(location.getColIndex(), capsules[0].length, "Col");
    return capsules[location.getRowIndex()][location.getColIndex()];
  }

  public boolean isGridEmpty() {
    for (int row = 0; row < capsules.length; row++) {
      for (int col = 0; col < capsules[row].length; col++) {
        if (capsules[row][col] != Capsule.EMPTY) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Finds locations that has same color capsule as the one in the selectedLocation.
   *
   * @param selectedLocation
   * @return {@link ImmutableSet} of {@link Location2D} that contain capsule of same color
   */
  @VisibleForTesting ImmutableSet<Location2D> findConnectedLocations(Location2D selectedLocation) {
    Capsule selectedCapsule = getCapsuleAt(selectedLocation);
    if (selectedCapsule == Capsule.EMPTY) {
      return ImmutableSet.of();
    }
    Color selectedColor = selectedCapsule.getColor();
    Set<Location2D> connectedLocations = new HashSet<>();
    Queue<Location2D> locations = new LinkedList<>();
    locations.add(selectedLocation);
    while (!locations.isEmpty()) {
      connectedLocations.add(selectedLocation);
      List<Location2D> subList = gridService.findCrossAdjacentLocations(locations.remove()).stream()
          .filter(location -> {
            Capsule capsule = getCapsuleAt(location);
            return capsule != Capsule.EMPTY && capsule.getColor() == selectedColor;
          })
          .filter(not(connectedLocations::contains))
          .collect(Collectors.toList());
      subList.forEach(loc -> {
        connectedLocations.add(loc);
        locations.add(loc);
      });
      log.info("locations size={}", locations.size());
    }
    return ImmutableSet.copyOf(connectedLocations);
  }

  /**
   * Creates a string representation of grid.
   */
  public String debugGetGrid() {
    StringBuilder gridString = new StringBuilder();
    for (int row = 0; row < capsules.length; row++) {
      for (int col = 0; col < capsules[row].length; col++) {
        Capsule capsule = capsules[row][col];
        gridString.append(capsule);
      }
      gridString.append('\n');
    }
    return gridString.toString();
  }
}
