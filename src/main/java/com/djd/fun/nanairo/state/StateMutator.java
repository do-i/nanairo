package com.djd.fun.nanairo.state;

public interface StateMutator {

  /**
   * Inceases time by the specified amount.
   *
   * @param seconds must be greater than equals to 1 second
   * @throws IllegalArgumentException if seconds < 1
   */
  void increaseTimeBy(int seconds);

  /**
   * Multiplies points by the specified factor
   */
  void multiplyPointsBy(int factor);

  /**
   * Reduces points by half
   *
   * @param factor
   */
  void reducePointsBy(int factor);

  /**
   * Reverses the gravity position.
   */
  void reverseGravity();


  /**
   * Forwards gravity position.
   */
  void rotateGravityForward();


  /**
   * Backwards gravity position.
   */
  void rotateGravityBackward();
}
