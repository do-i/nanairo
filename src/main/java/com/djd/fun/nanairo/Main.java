package com.djd.fun.nanairo;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.google.inject.Guice;
import com.google.inject.Injector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String [] args) {
    log.info("PoC for `nanairo-7` game. Mainly data structure, entities, models, algorithms.");
    SwingUtilities.invokeLater(() -> {
      Injector injector = Guice.createInjector(new GameModule());
      JFrame jFrame = injector.getInstance(JFrame.class);
      jFrame.pack();
      jFrame.setVisible(true);
    });

  }
}
