package com.djd.fun.nanairo.model;

import com.djd.fun.nanairo.state.StateMutator;

public enum Trait {

  NoOp {
    // no ops
    @Override public void apply(StateMutator stateMutator) {

    }
  },
  ChronoExpander {
    private static final int TIME_IN_SECONDS = 60;

    @Override public void apply(StateMutator stateMutator) {
      stateMutator.increaseTimeBy(TIME_IN_SECONDS);
    }
  },
  DoublePointer {
    private static final int FACTOR = 2;

    @Override public void apply(StateMutator stateMutator) {
      stateMutator.multiplyPointsBy(FACTOR);
    }
  },
  HalfPointer {
    private static final int FACTOR = 2;

    @Override public void apply(StateMutator stateMutator) {
      stateMutator.reducePointsBy(FACTOR);
    }
  },
  GravityReversal {
    @Override public void apply(StateMutator stateMutator) {
      stateMutator.reverseGravity();
    }
  },
  GravityForwardRotator {
    @Override public void apply(StateMutator stateMutator) {
      stateMutator.rotateGravityForward();
    }
  },
  GravityBackwardRotator {
    @Override public void apply(StateMutator stateMutator) {
      stateMutator.rotateGravityBackward();
    }
  };
  private static final int SIZE = Trait.values().length;

  public static int size() {
    return SIZE;
  }

  public abstract void apply(StateMutator stateMutator);

  @Override
  public String toString() {
    return String.valueOf(this.ordinal());
  }
}
