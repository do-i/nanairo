package com.djd.fun.nanairo.model;

public enum Color {
  Red('R', "#FF0000"),
  Orange('O', "#FF7F00"),
  Yellow('Y', "#FFFF00"),
  Green('G', "#00FF00"),
  Blue('B', "#0000FF"),
  Indigo('I', "#4B0082"),
  Violet('V', "#9400D3");

  private static final int SIZE = Color.values().length;
  private String hexCode;
  private char colorLetter;

  Color(char colorLetter, String hexCode) {
    this.colorLetter = colorLetter;
    this.hexCode = hexCode;
  }

  @Override
  public String toString() {
    return String.valueOf(colorLetter);
  }

  public String getHexCode() {
    return hexCode;
  }

  public static int size() {
    return SIZE;
  }

}
