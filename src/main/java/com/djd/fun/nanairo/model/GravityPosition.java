package com.djd.fun.nanairo.model;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

public enum GravityPosition {
  // tou, zai, nan, bok
  // E    W    S    N
  // Higa Nish Mina Kita

  Higa, Nish, Mina, Kita;

  private static final ImmutableMap<GravityPosition, GravityPosition> oppositions =
      Maps.immutableEnumMap(ImmutableMap.of(
          Higa, Nish,
          Nish, Higa,
          Mina, Kita,
          Kita, Mina));

  private static final ImmutableMap<GravityPosition, GravityPosition> forwardPositions =
      Maps.immutableEnumMap(ImmutableMap.of(
          Higa, Mina,
          Nish, Kita,
          Mina, Nish,
          Kita, Higa));

  private static final ImmutableMap<GravityPosition, GravityPosition> backwardPositions =
      Maps.immutableEnumMap(ImmutableMap.of(
          Higa, Kita,
          Nish, Mina,
          Mina, Higa,
          Kita, Nish));

  public GravityPosition getOpposit() {
    return oppositions.get(this);
  }

  public GravityPosition getForwardPosition() {
    return forwardPositions.get(this);
  }

  public GravityPosition getBackwardPosition() {
    return backwardPositions.get(this);
  }
}
