package com.djd.fun.nanairo.model;

public interface Capsule {
  Capsule EMPTY = new Capsule() {
    @Override public Color getColor() {
      throw new UnsupportedOperationException("No Color");
    }

    @Override public Trait getTrait() {
      throw new UnsupportedOperationException("No Trait");
    }

    @Override public String toString() {
      return "[E8]";
    }

  };

  Color getColor();

  Trait getTrait();
}
