package com.djd.fun.nanairo.model;

import java.util.Objects;

/**
 * A representation of an entity cell/capsule object on the game grid.
 */
public class CapsuleImpl implements Capsule {

  private final Color color;
  private final Trait trait;

  public CapsuleImpl(Builder builder) {
    this.color = builder.color;
    this.trait = builder.trait;
  }

  @Override public Color getColor() {
    return color;
  }

  @Override public Trait getTrait() {
    return trait;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static Builder builder(CapsuleImpl prototype) {
    return new Builder().fromPrototype(prototype);
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CapsuleImpl capsule = (CapsuleImpl)o;
    return Objects.equals(color, capsule.color) &&
        trait == capsule.trait;
  }

  @Override public int hashCode() {
    return Objects.hash(color, trait);
  }

  @Override public String toString() {
    return String.format("[%s%s]", color.toString(), trait.toString());
  }

  public static class Builder {
    private Color color;
    private Trait trait = Trait.NoOp;

    public Builder fromPrototype(CapsuleImpl prototype) {
      this.color = prototype.color;
      this.trait = prototype.trait;
      return this;
    }

    public Builder color(Color color) {
      this.color = color;
      return this;
    }

    public Builder trait(Trait trait) {
      this.trait = trait;
      return this;
    }

    public CapsuleImpl build() {
      return new CapsuleImpl(this);
    }
  }
}
