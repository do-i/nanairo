package com.djd.fun.nanairo.ui.sprite;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.inject.Singleton;

import com.djd.fun.io.MoreResources;
import com.djd.fun.nanairo.annotation.Red;
import com.djd.fun.nanairo.model.Color;
import com.djd.fun.nanairo.model.GravityPosition;
import com.djd.fun.nanairo.model.Trait;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Exposed;
import com.google.inject.PrivateModule;
import com.google.inject.Provides;

public class SpriteModule extends PrivateModule {

  @Override
  protected void configure() {

  }

  @Provides @Singleton @Exposed ImmutableMap<Color, BufferedImage> provideCapsuleSpriteByColorData() {
    return ImmutableMap.<Color, BufferedImage>builder()
        .put(Color.Red, load("mm_red.png"))
        .put(Color.Orange, load("mm_orange.png"))
        .put(Color.Yellow, load("mm_yellow.png"))
        .put(Color.Green, load("mm_green.png"))
        .put(Color.Blue, load("mm_blue.png"))
        .put(Color.Indigo, load("mm_teal.png"))
        .put(Color.Violet, load("mm_purple.png"))
        .build();
  }

  @Provides @Singleton @Exposed ImmutableMap<Trait, BufferedImage> provideTraitSpriteByTrait() {
    return ImmutableMap.<Trait, BufferedImage>builder()
        .put(Trait.GravityForwardRotator, load("tr_forward.png"))
        .put(Trait.GravityBackwardRotator, load("tr_backward.png"))
        .put(Trait.GravityReversal, load("tr_reverse.png"))
        .put(Trait.HalfPointer, load("tr_minus.png"))
        .put(Trait.DoublePointer, load("tr_plus.png"))
        .put(Trait.ChronoExpander, load("tr_chrono.png"))
        .build();
  }

  @Provides @Singleton @Exposed ImmutableMap<Integer, BufferedImage> provideDigitSpriteByInteger() {
    return ImmutableMap.<Integer, BufferedImage>builder()
        .put(0, load("digits/dg_0.png"))
        .put(1, load("digits/dg_1.png"))
        .put(2, load("digits/dg_2.png"))
        .put(3, load("digits/dg_3.png"))
        .put(4, load("digits/dg_4.png"))
        .put(5, load("digits/dg_5.png"))
        .put(6, load("digits/dg_6.png"))
        .put(7, load("digits/dg_7.png"))
        .put(8, load("digits/dg_8.png"))
        .put(9, load("digits/dg_9.png"))
        .build();
  }

  @Provides @Singleton @Exposed @Red ImmutableMap<Integer, BufferedImage> provideRedDigitSpriteByInteger() {
    return ImmutableMap.<Integer, BufferedImage>builder()
        .put(0, load("digits/red/dg_0.png"))
        .put(1, load("digits/red/dg_1.png"))
        .put(2, load("digits/red/dg_2.png"))
        .put(3, load("digits/red/dg_3.png"))
        .put(4, load("digits/red/dg_4.png"))
        .put(5, load("digits/red/dg_5.png"))
        .put(6, load("digits/red/dg_6.png"))
        .put(7, load("digits/red/dg_7.png"))
        .put(8, load("digits/red/dg_8.png"))
        .put(9, load("digits/red/dg_9.png"))
        .build();
  }

  @Provides @Singleton @Exposed ImmutableMap<GravityPosition, BufferedImage> provideArrowSpriteByGravityPosition() {
    return ImmutableMap.<GravityPosition, BufferedImage>builder()
        .put(GravityPosition.Kita, load("arrows/ar2_up.png"))
        .put(GravityPosition.Higa, load("arrows/ar2_right.png"))
        .put(GravityPosition.Mina, load("arrows/ar2_down.png"))
        .put(GravityPosition.Nish, load("arrows/ar2_left.png"))
        .build();
  }


  private static BufferedImage load(String name) {
    try {
      return ImageIO.read(MoreResources.getResource("images/" + name));
    } catch (IOException e) {
      throw new IllegalStateException("Unable to load capsule sprites.", e);
    }
  }
}
