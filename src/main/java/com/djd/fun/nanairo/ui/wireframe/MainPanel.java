package com.djd.fun.nanairo.ui.wireframe;

import java.awt.BorderLayout;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JPanel;

import com.djd.fun.nanairo.ui.game.GamePanel;
import com.djd.fun.nanairo.ui.game.StatusPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class MainPanel extends JPanel {

  private static final Logger log = LoggerFactory.getLogger(MainPanel.class);

  @Inject
  public MainPanel(BorderLayout layout, GamePanel gamePanel, StatusPanel statusPanel) {
    setLayout(layout);
    add(statusPanel, BorderLayout.PAGE_START);
    add(gamePanel, BorderLayout.CENTER);
    log.info("init");
  }
}
