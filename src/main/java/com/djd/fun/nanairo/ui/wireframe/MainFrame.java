package com.djd.fun.nanairo.ui.wireframe;

import javax.inject.Singleton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.djd.fun.nanairo.ui.wireframe.util.Fonts;
import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class MainFrame extends JFrame {

  private static final Logger log = LoggerFactory.getLogger(MainFrame.class);
  private static final String FRAME_TITLE = "Nanairo";

  @Inject
  public MainFrame(MainPanel mainPanel, MenuBar menuBar) {
    super(FRAME_TITLE);
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setJMenuBar(menuBar);
    add(mainPanel);
    Fonts.configure();
    setResizable(false);
    log.info("init JFrame");
  }
}
