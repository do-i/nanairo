package com.djd.fun.nanairo.ui.wireframe;

import java.awt.Font;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;

import com.djd.fun.nanairo.ui.event.NewGameEvent;
import com.google.common.eventbus.EventBus;

@Singleton
public class MenuBar extends JMenuBar {

  @Inject
  public MenuBar(EventBus eventBus) {
    Font f = new Font("sans-serif", Font.PLAIN, 12);
    UIManager.put("Menu.font", f);
    UIManager.put("MenuItem.font", f);
    super.add(createFileMenu(eventBus));
  }

  private static JMenu createFileMenu(EventBus eventBus) {
    JMenu jMenuFile = new JMenu("Game");
    JMenuItem jMenuItemNew = new JMenuItem("New");
    jMenuItemNew.addActionListener(event -> eventBus.post(new NewGameEvent()));
    jMenuFile.add(jMenuItemNew);
    JMenuItem jMenuItemExit = new JMenuItem("Exit");
    jMenuItemExit.addActionListener(event -> System.exit(0));
    jMenuFile.add(jMenuItemExit);
    return jMenuFile;
  }
}
