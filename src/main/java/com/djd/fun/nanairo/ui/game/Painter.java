package com.djd.fun.nanairo.ui.game;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

@Singleton
public class Painter {

  private List<Component> components;

  public Painter() {
    components = new ArrayList<>();
  }

  public boolean addComponent(Component component) {
    return components.add(component);
  }

  public boolean removeComponent(Component component) {
    return components.remove(component);
  }

  public void paint() {
    components.forEach(Component::repaint);
  }
}
