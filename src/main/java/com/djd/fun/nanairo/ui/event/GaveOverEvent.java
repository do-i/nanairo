package com.djd.fun.nanairo.ui.event;

public class GaveOverEvent {
  private final String message;

  public GaveOverEvent(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
