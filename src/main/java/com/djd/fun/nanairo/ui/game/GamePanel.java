package com.djd.fun.nanairo.ui.game;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.event.MouseInputAdapter;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.base.Location2D;
import com.djd.fun.nanairo.annotation.BasePanelSize;
import com.djd.fun.nanairo.annotation.Cell;
import com.djd.fun.nanairo.annotation.Grid;
import com.djd.fun.nanairo.annotation.MaxScore;
import com.djd.fun.nanairo.model.Capsule;
import com.djd.fun.nanairo.model.Color;
import com.djd.fun.nanairo.model.Trait;
import com.djd.fun.nanairo.state.GameGrid;
import com.djd.fun.nanairo.ui.event.GaveOverEvent;
import com.djd.fun.nanairo.ui.event.NewGameEvent;
import com.djd.fun.nanairo.ui.wireframe.Abstract2DPanel;
import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class GamePanel extends Abstract2DPanel {

  private static final Logger log = LoggerFactory.getLogger(GamePanel.class);
  private final GameGrid gameGrid;
  private final ImmutableSize panelSize;
  private final ImmutableSize gridSize;
  private final ImmutableSize renderTileSize;
  private final int maxScore;
  private final ImmutableMap<Color, BufferedImage> capsuleSpriteByColor;
  private final ImmutableMap<Trait, BufferedImage> traitSpriteByTrait;
  private final EventBus eventBus;
  private final MouseInput mouseInput;

  /* statefull object */
  private final Painter painter;

  @Inject
  GamePanel(GameGrid gameGrid, @BasePanelSize ImmutableSize panelSize,
      @Grid ImmutableSize gridSize,
      @Cell ImmutableSize renderTileSize,
      @MaxScore int maxScore,
      ImmutableMap<Color, BufferedImage> capsuleSpriteByColor,
      ImmutableMap<Trait, BufferedImage> traitSpriteByTrait,
      Painter painter,
      EventBus eventBus) {
    this.gameGrid = gameGrid;
    this.panelSize = panelSize;
    this.gridSize = gridSize;
    this.renderTileSize = renderTileSize;
    this.maxScore = maxScore;
    this.capsuleSpriteByColor = capsuleSpriteByColor;
    this.traitSpriteByTrait = traitSpriteByTrait;
    this.eventBus = eventBus;
    this.painter = painter;
    this.painter.addComponent(this);
    this.mouseInput = new MouseInput();
    super.addMouseListener(mouseInput);
  }

  @Subscribe
  public void startNewGame(NewGameEvent event) {
    gameGrid.init();
    painter.paint();
  }

  @Override
  public Dimension getPreferredSize() {
    return panelSize.toDimension();
  }

  @Override
  protected void paintComponent(Graphics2D g) {
    paintCapsules(g);
  }

  private void paintCapsules(Graphics2D g) {
    for (int row = 0; row < gridSize.getHeight(); row++) {
      for (int col = 0; col < gridSize.getWidth(); col++) {
        Location2D location = Location2D.of(row, col);
        Capsule capsule = gameGrid.getCapsuleAt(location);
        if (capsule != Capsule.EMPTY) {
          drawSprite(g, capsuleSpriteByColor.get(capsule.getColor()), location);
          Trait trait = capsule.getTrait();
          if (trait != Trait.NoOp) {
            BufferedImage traitSprite = traitSpriteByTrait.get(trait);
            if (traitSprite == null) {
              throw new IllegalStateException("Missing trait sprite for " + trait);
            }
            drawTraitSprite(g, traitSprite, location);
          }
        }
      }
    }
  }

  private void drawSprite(Graphics2D g, BufferedImage sprite, Location2D location) {
    g.drawImage(sprite,
        location.getColIndex() * renderTileSize.getWidth(),
        location.getRowIndex() * renderTileSize.getHeight(),
        renderTileSize.getWidth(),
        renderTileSize.getHeight(),
        this);
  }

  private void drawTraitSprite(Graphics2D g, BufferedImage sprite, Location2D location) {
    int delta = renderTileSize.getWidth() / 4;
    g.drawImage(sprite,
        location.getColIndex() * renderTileSize.getWidth() + delta,
        location.getRowIndex() * renderTileSize.getHeight() + delta,
        renderTileSize.getWidth() / 2,
        renderTileSize.getHeight() / 2,
        this);
  }

  private class MouseInput extends MouseInputAdapter {

    @Override
    public void mouseClicked(MouseEvent event) {
      Location2D location = Location2D.of(
          event.getY() / renderTileSize.getHeight(),
          event.getX() / renderTileSize.getWidth());
      gameGrid.handleTileSelection(location);
      if (gameGrid.getActivePoints() >= maxScore) {
        eventBus.post(new GaveOverEvent("You won by reaching maximum possible score."));
      }
      painter.paint();
      if (gameGrid.isGridEmpty()) {
        eventBus.post(new GaveOverEvent("Gave over!"));
      }
    }
  }
}
