package com.djd.fun.nanairo.ui.game;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.time.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import com.djd.fun.base.ImmutableSize;
import com.djd.fun.base.Location2D;
import com.djd.fun.nanairo.annotation.Cell;
import com.djd.fun.nanairo.annotation.Red;
import com.djd.fun.nanairo.annotation.StatusPanelSize;
import com.djd.fun.nanairo.model.GravityPosition;
import com.djd.fun.nanairo.state.GameGrid;
import com.djd.fun.nanairo.ui.event.GaveOverEvent;
import com.djd.fun.nanairo.ui.event.NewGameEvent;
import com.djd.fun.nanairo.ui.wireframe.Abstract2DPanel;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.Subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class StatusPanel extends Abstract2DPanel {

  private static final Logger log = LoggerFactory.getLogger(StatusPanel.class);
  private final GameGrid gameGrid;
  private final ImmutableSize panelSize;
  private final ImmutableSize renderTileSize;
  private final ImmutableMap<Integer, BufferedImage> digitSpriteByInt;
  private final ImmutableMap<Integer, BufferedImage> redDigitSpriteByInt;
  private final ImmutableMap<GravityPosition, BufferedImage> arrowSpriteByGravityPosition;
  private final Timer timer;

  /* stateful object */
  private final Painter painter;

  @Inject StatusPanel(GameGrid gameGrid, @StatusPanelSize ImmutableSize panelSize,
      @Cell ImmutableSize renderTileSize,
      ImmutableMap<Integer, BufferedImage> digitSpriteByInt,
      @Red ImmutableMap<Integer, BufferedImage> redDigitSpriteByInt,
      ImmutableMap<GravityPosition, BufferedImage> arrowSpriteByGravityPosition,
      Painter painter) {
    this.gameGrid = gameGrid;
    this.panelSize = panelSize;
    this.renderTileSize = renderTileSize;
    this.digitSpriteByInt = digitSpriteByInt;
    this.redDigitSpriteByInt = redDigitSpriteByInt;
    this.arrowSpriteByGravityPosition = arrowSpriteByGravityPosition;
    this.painter = painter;
    this.painter.addComponent(this);
    this.timer = new Timer(0, (event) -> {
      checkGameStatus();
      repaint();
    });
    timer.setDelay(1000); // refresh every second
    timer.start();
  }

  @Subscribe
  public void startNewGame(NewGameEvent event) {
    gameGrid.init();
    painter.paint();
  }

  @Override
  public Dimension getPreferredSize() {
    return panelSize.toDimension();
  }

  @Override
  protected void paintComponent(Graphics2D g) {
    int colOffset = paintTime(g, 0);
    drawSprite(g, arrowSpriteByGravityPosition.get(gameGrid.getActiveGravityPosition()),
        Location2D.of(0, colOffset++));
    for (BufferedImage digitSprite : convertScoreToSprites()) {
      drawSprite(g, digitSprite, Location2D.of(0, colOffset++));
    }
  }

  private void checkGameStatus() {
    if (gameGrid.getActiveRemainingDuration().isNegative()) {
      showDialog(new GaveOverEvent("Time's up!"));
    }
  }

  @Subscribe
  public void showDialog(GaveOverEvent event) {
    timer.stop();
    JOptionPane.showMessageDialog(this, event.getMessage() + " Reinitialize the Board");
    gameGrid.init();
    painter.paint();
    timer.start();
  }

  private int paintTime(Graphics2D g, int colOffset) {
    for (BufferedImage digitSprite : convertTimeToSprites()) {
      drawSprite(g, digitSprite, Location2D.of(0, colOffset++));
    }
    return colOffset;
  }

  private void drawSprite(Graphics2D g, BufferedImage sprite, Location2D location) {
    g.drawImage(sprite,
        location.getColIndex() * renderTileSize.getWidth(),
        location.getRowIndex() * renderTileSize.getHeight(),
        renderTileSize.getWidth(),
        renderTileSize.getHeight(),
        this);
  }

  private ImmutableList<BufferedImage> convertScoreToSprites() {
    return convertDigitsToSprites(digitSpriteByInt, String.format("%07d",
        gameGrid.getActivePoints()));
  }

  private ImmutableList<BufferedImage> convertTimeToSprites() {
    Duration timeRemain = gameGrid.getActiveRemainingDuration();
    if (timeRemain.isNegative()) {
      timeRemain = Duration.ZERO;
    }
    return convertDigitsToSprites(redDigitSpriteByInt, String.format("%03d",
        timeRemain.getSeconds()));
  }

  private static ImmutableList<BufferedImage> convertDigitsToSprites(
      ImmutableMap<Integer, BufferedImage> spritesByDigit, String digits) {
    ImmutableList.Builder<BufferedImage> scoreSprites = ImmutableList.builder();
    for (char digit : digits.toCharArray()) {
      scoreSprites.add(spritesByDigit.get(Character.getNumericValue(digit)));
    }
    return scoreSprites.build();
  }
}
