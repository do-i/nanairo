package com.djd.fun.nanairo.ui.wireframe;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public abstract class Abstract2DPanel extends JPanel {

  @Override
  protected final void paintComponent(Graphics g) {
    super.paintComponent(g);
    paintComponent((Graphics2D)g);
    requestFocusInWindow(); // NOTE: This enables KeyListener on JPanel. This has to be called after JFrame is set to visible
  }

  abstract protected void paintComponent(Graphics2D g);

}
