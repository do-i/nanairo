package com.djd.fun.base;

import static java.lang.Math.abs;

public class Location2D extends IntegerPair {

  private Location2D(int row, int col) {
    super(row, col);
  }

  public int getRowIndex() {
    return value1;
  }

  public int getColIndex() {
    return value2;
  }

  public int computeManhattanDistance(Location2D other) {
    int rowDistance = abs(getRowIndex() - other.getRowIndex());
    int colDistance = abs(getColIndex() - other.getColIndex());
    return rowDistance + colDistance;
  }

  public static Location2D of(int row, int col) {
    return new Location2D(row, col);
  }
}
