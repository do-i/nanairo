package com.djd.fun.base;


import java.awt.Dimension;

/**
 *
 */
public class ImmutableSize extends IntegerPair {

  private ImmutableSize(Builder builder) {
    super(builder.width, builder.height);
  }

  public int getWidth() {
    return value1;
  }

  public int getHeight() {
    return value2;
  }

  /**
   * @return creates new instance of {@link Dimension} with current instance of size values
   */
  public Dimension toDimension() {
    return new Dimension(getWidth(), getHeight());
  }

  public static ImmutableSize of(int width, int height) {
    return builder().width(width).height(height).build();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    int width;
    int height;

    public Builder width(int width) {
      this.width = width;
      return this;
    }

    public Builder height(int height) {
      this.height = height;
      return this;
    }

    public ImmutableSize build() {
      return new ImmutableSize(this);
    }
  }

}
